[75.08 - Sistemas Operativos] [FIUBA] [Grupo 5] [2do cuatrimeste 2016] 

*Guardar en el archivo comprimido en un directorio a eleccion

*Para descomprimir el paquete posicionese en el directorio elegido anteriormente y ejecute
$tar -xvzf grupo5-tp-EPLAM-2do2016-7508.tgz [-C /ruta/destino]
(En caso de NO proporcionar un ruta de destino, el paquete sera descomprimido en el directorio donde se encuentra el mismo)

*Luego de la descompresion se generara un directorio dirconf 

*Para comenzar la instalacion ejecute 
$./Instalep.sh
(De no tener los permisos necesarios ejecute chmod +x Instalep.sh y vuelva a intentar)

*Seguir las intrucciones que figuran en pantalla, a lo largo de la intalacion se pedira que defina distintos directorios para la 
si no ingresa ninguno el sistema tomara por default los directorios indicados entre parentesis 
Restricciones: ninguno de los directorios debe llevar el nombre "dirconf", el intalador no se lo permitira.

*Luego de la intalacion se creearan todos los dictoriorios mencionados anteriormente y además los archivos maestros y tablas

*Para comenzar la ejecucion del sistema ir a al directorio donde estan los archivos binarios y ejecutar 
$source ./initep.sh
(Tip: En caso de no tener los permisos necesarios ejecute el siguiente comando: $chmod +x initep.sh y vuelva a intentar)

*El initep proporciona la opcion de arrancar el comando deamon de forma automatica.
Para la ejecucion de forma manual ejecute:
$./deamonep.sh

*Es indispesable la correcta instalacion e inicializacion del sistema antes de seguir con la ejecucion de cualquier comando.

